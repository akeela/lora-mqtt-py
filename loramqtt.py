#!/usr/bin/env python3
#using: utf-8

import sys, time
import paho.mqtt.client as mqtt
import json

from modules import modules

""" mqtt config """
BROKER_ADDR = "127.0.0.1"
TOPIC = "devices/lora/"

def convert_to_msg(recv):
	if (recv[0] != "I"):
		print ("Recivied something strange from {0} node".format(recv[1:17]))
		print ("Don\'t know what to do...")
		return -1
	deveui = recv [1:17]
	rssi = -(256-int(recv[19:21],16))
	module_id = int (recv[23:25],16)
	modules.modules[module_id](recv[25:])
	return {
		"data": 
		modules.modules[module_id](recv[25:]),
		"status": 
		{
			"devEUI": ("{0}".format(deveui)),
			"rssi": ("{0}".format(rssi)),
			"temperature": "0",
			"battery": "0",
			"date": (time.strftime("%Y-%m-%dT%H:%M:%S.000000Z"))
		}
	}

def mqtt_publish(msg):
	client = mqtt.Client("lora_gateway")
	client.connect(BROKER_ADDR,1883)
	client.publish(TOPIC, msg)

def recv_send(bytemsglist):
	try:
		bytemsg = (bytemsglist.split())
		print (bytemsg)
		for i in bytemsg:
			msg = (str(convert_to_msg(i)))
			if (msg != '-1'):
				print ("Got: {0}".format(i))
				print ("Publish message")
				mqtt_publish(json.dumps(msg))
				print ("Done!")
	except Exception as e:
		print (e)
	finally:
		pass

def main():
	if (len(sys.argv)>=2):
		recv_send(sys.argv[1])

if __name__ == '__main__':
	main()