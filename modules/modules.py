def bme280(recv):
	temp = ("{0}".format(round(int((recv[2:4] + recv[0:2]),16)*0.1,1)))
	hum = ("{0}".format(round(int((recv[6:8] + recv[4:6]),16)*0.1,1)))
	press = ("{0}".format(int((recv[10:12] + recv[8:10]),16)))
	return {
		"temperature": temp,
		"humidity": hum,
		"pressure": press
	}

def default(recv):
	return {
		"joined": int((recv[2:4] + recv[0:2]),16),
		"class": int((recv[6:8] + recv[4:6]),16)
	}

modules = {
	0: default,
	17: bme280
}

