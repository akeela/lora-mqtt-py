#!/usr/bin/env python3
#using: utf-8

import sys, time
import serial

import loramqtt

""" serial config """
SERIALPORT = '/dev/ttyUSB0'
BAUDRATE = 115200
DELAY = 5 # send 'P' command every 5 seconds

# uwrd-lora commands
"""
 	CMD_PING = 'P',				/* Command to ping/pong with client */
	CMD_DEVLIST = 'L',			/* Command to get devices list from a gate */
	CMD_IND = 'I',				/* Individual command to the mote by address */
	CMD_HAS_PENDING = '?',		/* Individual device has N pending packets */
	CMD_INVITE = 'V',			/* Individual invite to join network for class C devices */
	CMD_BROADCAST = 'B',		/* Broadcast message */

	CMD_ADD_STATIC_DEV = 'A',	/* Sets nonce from which key will be derived for the specified network address and channel (for statically personalized devices) */
	CMD_KICK_ALL_STATIC = 'K',	/* Kicks device by specified network address via removing it from devices list */

	CMD_FLUSH = 'F',			/* Command to get all pending info */
    
    CMD_SET_REGION = 'G',
    CMD_SET_CHANNEL = 'C',
    CMD_SET_DATARATE = 'D',
    CMD_SET_JOINKEY = 'J',
    CMD_REBOOT = 'R',
    CMD_FW_UPDATE = 'U'
"""

ser=serial.Serial()
ser.port= SERIALPORT
ser.baudrate= BAUDRATE
ser.timeout=0
ser.parity=serial.PARITY_NONE
ser.bytesize=serial.EIGHTBITS
ser.stopbits=serial.STOPBITS_ONE

def uart_write():
	ser.write(b'P\x0A\x0D')

def uart_read():
	out = b''
	time.sleep(DELAY)
	while ser.inWaiting() > 0:
		out += ser.read(1)
	return out.decode("utf-8")

if __name__	== "__main__": 
	try:
		b = b''
		ser.open()
		ser.flushInput()
		ser.flushOutput()
		while (1):
			uart_write()
			b = uart_read()
			b = b.strip()
			print (b)
			if (len(b) > 2):
				loramqtt.recv_send(b)
		ser.close()
	except Exception as e:
		print (e)
	finally:
		ser.close()